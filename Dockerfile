# Maven build container 

#FROM maven:3.6.3-openjdk-11 AS maven_build

#COPY pom.xml /tmp/

#COPY src /tmp/src/

#WORKDIR /tmp/

#RUN mvn package

#pull base image

##-------------Normal workflow--------##

#[FROM openjdk:latest

#copy hello world to docker image from builder image

#COPY target/hello-world-0.1.0.jar /data/hello-world-0.1.0.jar

#maintainer 
#MAINTAINER dstar55@yahoo.com
#expose port 8080
#EXPOSE 8080

#default command
#CMD java -jar /data/hello-world-0.1.0.jar]

##----------For dockerfile-maven-plugin implementation ------##

FROM openjdk:latest
#RUN addgroup -S spring && adduser -S spring -G spring
#USER spring:spring
#VOLUME /tmp
ARG JAR_FILE
ADD ${JAR_FILE} /app/app.jar
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/app.jar"]


